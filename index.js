/*
Client-server architecture
Node.js Introduction
*/

// multiple clients may communicate with a single server
// only clients may initiate communication with a server
// clients may not communicate directly with each other

/*
Benefits
 -scalable
 -maintainable
 -multiple clients may all use dynamically-generated data
 -workload concentrated on server ==> clients apps lightweight
 -improves data availability
*/

// Node.js for building server-side applications

// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (nodeJS/expressJS application) communicate by exchanging individual messages (requests/responses).
// The message sent by the client is called "request".]
// The message sent by the server as an answer is called "response".

let http = require("http");

// The "http module" has a createServer() method that accepts a function as argument and allows for a creation of a server
// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
// createServer() is a method of the http object responsible for creating a server using Node.js

						// req, res
						// request, response
http.createServer(function(x, response){
	  // Use the writeHead() method to :
		// Set a status code for the response. (200 means okay)
			// HTTP Response status codes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
		// Set the content-type of the response

	response.writeHead(200, {"Content-Type" : "text/plain"});

	response.end("Hello World");

}).listen(3000); // A port is a virtual point where network connections start and end
// Each port is associated with specific process or services
// The server will be assigned to port 3000 via the listen() method
	// where the server will listen to any request that sent to it and will also send the response via this port.
// 3000, 4000, 5000, 8000 - usually used for web development

console.log("Server is running at localhost:3000");
console.log("You may now use the application");
console.log("Welcome to my application");
// We will acces the server in the browser using the localhost:4000

// Command to run our web application (to be used in our gitbash):
/*
	1. Install nodemon packages
	-npm install -g nodemon
		-npm i -g nodemon // shortcut
	-sudo npm install -g nodemon

	2. Once nodemon packages are already installed/downloaded, you may not install the nodemon again
	- nodemon (file to access)
*/